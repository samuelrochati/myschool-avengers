import dotenv from 'dotenv/config';
import '@babel/polyfill';
import setupApp from './src/app';
import Config from './src/config/config';

const port = process.env.PORT ? process.env.PORT : Config.PORT;

setupApp()
  .then(app => app.listen(port, () => console.log('app running on port ' + port)))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });
