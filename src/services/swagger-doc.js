const options = {
  swaggerDefinition: {
    // Like the one described here: https://swagger.io/specification/#infoObject
    info: {
      title: 'API MySchool',
      version: '1.0.0',
      description: 'Documentation API MySchool com Swagger Docs'
    },
    schemes: ['https', 'http'],
    host: 'http://api-gateway-myschool.herokuapp.com',
    basePath: '/gestor',
    paths: {
      '/curso': {
        get: {
          tags: ['curso'],
          summary: 'Find all curso',
          description: 'Returns all curso',
          produces: ['application/json'],
          responses: {
            '200': {
              description: 'successful operation'
            },
            '400': {
              description: 'Invalid status value'
            }
          }
        }
      },
      '/curso': {
        post: {
          tags: ['curso'],
          summary: 'Add a new curso',
          description: '',
          produces: ['application/json'],
          consumes: ['application/x-www-form-urlencoded'],
          parameters: [
            {
              in: 'body',
              name: 'body',
              description: 'Curso object that needs to be added',
              required: true
            }
          ],
          responses: {
            '201': {
              description: 'successful operation'
            },
            '422': {
              description: 'unprocessable entity'
            }
          }
        }
      },
      '/curso/{cursoId}': {
        get: {
          tags: ['curso'],
          summary: 'Find curso by ID',
          description: 'Returns a single curso',
          produces: ['application/json'],
          parameters: [
            {
              name: 'cursoId',
              in: 'path',
              description: 'ID of curso to return',
              required: true
            }
          ],
          responses: {
            '200': {
              description: 'successful operation'
            },
            '400': {
              description: 'Invalid ID supplied'
            }
          }
        }
      },
      '/curso/{cursoIdEdit}': {
        put: {
          tags: ['curso'],
          summary: 'Edit curso by ID',
          description: 'Edit a single curso',
          produces: ['application/json'],
          parameters: [
            {
              name: 'cursoIdEdit',
              in: 'path',
              description: 'ID of curso to return',
              required: true
            }
          ],
          responses: {
            '200': {
              description: 'successful operation'
            },
            '400': {
              description: 'Invalid ID supplied'
            }
          }
        }
      },
      '/curso/{cursoIdDelete}': {
        delete: {
          tags: ['curso'],
          summary: 'Delete curso by ID',
          description: 'Delete a single curso',
          produces: ['application/json'],
          parameters: [
            {
              name: 'cursoIdDelete',
              in: 'path',
              description: 'ID of curso to return',
              required: true
            }
          ],
          responses: {
            '200': {
              description: 'successful operation'
            },
            '400': {
              description: 'Invalid ID supplied'
            }
          }
        }
      },
      '/disciplina': {
        get: {
          tags: ['disciplina'],
          summary: 'Find all disciplina',
          description: 'Returns all disciplina',
          produces: ['application/json'],
          responses: {
            '200': {
              description: 'successful operation'
            },
            '400': {
              description: 'Invalid status value'
            }
          }
        }
      },
      '/disciplina': {
        post: {
          tags: ['disciplina'],
          summary: 'Add a new disciplina',
          description: '',
          produces: ['application/json'],
          consumes: ['application/x-www-form-urlencoded'],
          parameters: [
            {
              in: 'body',
              name: 'body',
              description: 'Disciplina object that needs to be added',
              required: true
            }
          ],
          responses: {
            '201': {
              description: 'successful operation'
            },
            '422': {
              description: 'unprocessable entity'
            }
          }
        }
      },
      '/disciplina/{disciplinaId}': {
        get: {
          tags: ['disciplina'],
          summary: 'Find disciplina by ID',
          description: 'Returns a single disciplina',
          produces: ['application/json'],
          parameters: [
            {
              name: 'disciplinaId',
              in: 'path',
              description: 'ID of disciplina to return',
              required: true
            }
          ],
          responses: {
            '200': {
              description: 'successful operation'
            },
            '400': {
              description: 'Invalid ID supplied'
            }
          }
        }
      },
      '/disciplina/{disciplinaIdEdit}': {
        put: {
          tags: ['disciplina'],
          summary: 'Edit disciplina by ID',
          description: 'Edit a single disciplina',
          produces: ['application/json'],
          parameters: [
            {
              name: 'disciplinaIdEdit',
              in: 'path',
              description: 'ID of disciplina to return',
              required: true
            }
          ],
          responses: {
            '200': {
              description: 'successful operation'
            },
            '400': {
              description: 'Invalid ID supplied'
            }
          }
        }
      },
      '/disciplina/{disciplinaIdDelete}': {
        delete: {
          tags: ['disciplina'],
          summary: 'Delete disciplina by ID',
          description: 'Delete a single disciplina',
          produces: ['application/json'],
          parameters: [
            {
              name: 'disciplinaIdDelete',
              in: 'path',
              description: 'ID of disciplina to return',
              required: true
            }
          ],
          responses: {
            '200': {
              description: 'successful operation'
            },
            '400': {
              description: 'Invalid ID supplied'
            }
          }
        }
      },
      '/turma': {
        get: {
          tags: ['turma'],
          summary: 'Find all turma',
          description: 'Returns all turma',
          produces: ['application/json'],
          responses: {
            '200': {
              description: 'successful operation'
            },
            '400': {
              description: 'Invalid status value'
            }
          }
        }
      },
      '/turma': {
        post: {
          tags: ['turma'],
          summary: 'Add a new turma',
          description: '',
          produces: ['application/json'],
          consumes: ['application/x-www-form-urlencoded'],
          parameters: [
            {
              in: 'body',
              name: 'body',
              description: 'Turma object that needs to be added',
              required: true
            }
          ],
          responses: {
            '201': {
              description: 'successful operation'
            },
            '422': {
              description: 'unprocessable entity'
            }
          }
        }
      },
      '/turma/{turmaId}': {
        get: {
          tags: ['turma'],
          summary: 'Find turma by ID',
          description: 'Returns a single turma',
          produces: ['application/json'],
          parameters: [
            {
              name: 'turmaId',
              in: 'path',
              description: 'ID of turma to return',
              required: true
            }
          ],
          responses: {
            '200': {
              description: 'successful operation'
            },
            '400': {
              description: 'Invalid ID supplied'
            }
          }
        }
      },
      '/turma/{turmaIdEdit}': {
        put: {
          tags: ['turma'],
          summary: 'Edit turma by ID',
          description: 'Edit a single turma',
          produces: ['application/json'],
          parameters: [
            {
              name: 'turmaIdEdit',
              in: 'path',
              description: 'ID of turma to return',
              required: true
            }
          ],
          responses: {
            '200': {
              description: 'successful operation'
            },
            '400': {
              description: 'Invalid ID supplied'
            }
          }
        }
      },
      '/turma/{turmaIdDelete}': {
        delete: {
          tags: ['turma'],
          summary: 'Delete turma by ID',
          description: 'Delete a single turma',
          produces: ['application/json'],
          parameters: [
            {
              name: 'turmaIdDelete',
              in: 'path',
              description: 'ID of turma to return',
              required: true
            }
          ],
          responses: {
            '200': {
              description: 'successful operation'
            },
            '400': {
              description: 'Invalid ID supplied'
            }
          }
        }
      }
    },
    definitions: {
      Curso: {
        type: 'object',
        properties: {
          _id: {
            type: 'integer',
            format: 'int64'
          },
          name: {
            type: 'string'
          }
        },
        xml: {
          name: 'Curso'
        }
      },
      Disciplina: {
        type: 'object',
        properties: {
          _id: {
            type: 'integer',
            format: 'int64'
          },
          name: {
            type: 'string'
          },
          ch: {
            type: 'integer'
          },
          idcurso: {
            type: 'integer',
            format: 'int64'
          }
        },
        xml: {
          name: 'Disciplina'
        }
      },
      Turma: {
        type: 'object',
        properties: {
          _id: {
            type: 'integer',
            format: 'int64'
          },
          codigo: {
            type: 'string'
          },
          sala: {
            type: 'string'
          },
          vagas: {
            type: 'integer'
          },
          idcurso: {
            type: 'integer',
            format: 'int64'
          }
        },
        xml: {
          name: 'Turma'
        }
      }
    }
  },
  // List of files to be processes. You can also set globs './routes/*.js'
  apis: ['./routes/*.js']
};

export default options;
