import axios from 'axios';

const api = axios.create({
  baseURL: 'https://api-myschool.herokuapp.com/users'
});

export default api;
