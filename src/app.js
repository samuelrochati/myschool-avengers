import express from 'express';
import bodyParser from 'body-parser';
import routes from './routes';
import database from './config/database';
import options from './services/swagger-doc';

const app = express();

// Swagger Documentation
const swaggerJsdoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

const specs = swaggerJsdoc(options);

const configureExpress = () => {
  app.use(bodyParser.json());

  app.use((req, res, next) => {
    console.log(`${new Date().toString()} => ${req.originalUrl}`, req.body);
    next();
  });

  app.use('/', routes);
  app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(specs));

  return app;
};

export default () => database.connect().then(configureExpress);
